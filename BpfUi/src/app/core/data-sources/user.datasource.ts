import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { UserModel } from '../models/user.model';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { UserService } from '../services/user.service';
import { catchError, finalize } from 'rxjs/operators';
import { ApiResponse } from 'src/app/shared/models/api-response.model';
import { TableData } from 'src/app/shared/models/table-data.model';
import { PagingData } from 'src/app/shared/models/paging-data';
import { GlobalHelper } from 'src/app/shared/global-helper';

export class UserDataSource extends DataSource<UserModel> {

    private userSubject = new BehaviorSubject<UserModel[]>([]);
    /* private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading = this.loadingSubject.asObservable(); */
    public length = 0;
    public pageSize = GlobalHelper.tableConfig.pageSize;
    public pageSizeOptions = GlobalHelper.tableConfig.pageSizeOptions;

    constructor(private userService: UserService) {
        super();
    }

    connect(collectionViewer: CollectionViewer): Observable<UserModel[]> {
        return this.userSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.userSubject.complete();
        // this.loadingSubject.complete();
    }

    loadUsers(pagingData: PagingData) {
        // this.loadingSubject.next(true);
        this.length = 0;

        this.userService.getByFilter(pagingData)
        .pipe(
            catchError(() => of([]))
            /* finalize(() => {
                this.loadingSubject.next(false);
            }) */
        )
        .subscribe((data: ApiResponse<TableData<UserModel>>) => {
            if (!data.hasError) {
                this.length = data.result.totalRecords;
                this.userSubject.next(data.result.pageRecords);
              } else {
                this.userSubject.next([]);
              }
        });
    }
}
