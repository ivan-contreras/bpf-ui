import { Injectable } from '@angular/core';
import { GlobalHelper } from '../../shared/global-helper';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotifierService {
    constructor(private toasterService: ToastrService) { }

    showToasterMessage(hasError: boolean, message: string, title: string = null) {
        const messageTitle = title || GlobalHelper.appName;
        if (hasError) {
            this.toasterService.error(message, messageTitle);
        } else {
            this.toasterService.success(message, messageTitle);
        }
    }

    showToasterSuccessMessage(message: string, title: string = null) {
        const messageTitle = title || GlobalHelper.appName;
        this.toasterService.success(message, messageTitle);
    }

    showToasterInfoMessage(message: string, title: string = null) {
        const messageTitle = title || GlobalHelper.appName;
        this.toasterService.info(message, messageTitle);
    }

    showToasterErrorMessage(message: string, title: string = null) {
        const messageTitle = title || GlobalHelper.appName;
        this.toasterService.error(message, messageTitle);
    }
}
