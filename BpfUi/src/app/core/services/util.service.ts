import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable()
export class UtilService {
    constructor() { }

    logValidationErrors(group: FormGroup, validationMessages: object, submitted: boolean, formErrors: object) {
        Object.keys(group.controls).forEach((key: string) => {
            const abstractControl = group.get(key);
            formErrors[key] = '';

            if (abstractControl && !abstractControl.valid &&
                (abstractControl.touched || abstractControl.dirty || abstractControl.value || submitted)) {
                const messages = validationMessages[key];
                for (const errorKey in abstractControl.errors) {
                    if (errorKey) {
                        formErrors[key] += messages[errorKey] + ' ';
                    }
                }
            }

            if (abstractControl instanceof FormGroup) {
                this.logValidationErrors(abstractControl, validationMessages, submitted, formErrors);
            }
        });
    }
}
