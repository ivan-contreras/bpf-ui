import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { LoginModel } from '../../modules/login/login.model';
import { AccountModel } from '../../shared/models/account.model';
import { GlobalHelper } from '../../shared/global-helper';
import { ApiResponse } from '../../shared/models/api-response.model';

@Injectable()
export class AuthService {
  private servicePath = `${GlobalHelper.serverPath}Security`;

  constructor(private http: HttpClient) {}

  loginUser(loginCredentials: LoginModel): Observable<ApiResponse<AccountModel>> {
    const url = `${this.servicePath}/Login`;
    return this.http.post<ApiResponse<AccountModel>>(url, loginCredentials);
  }

  amIAuthenticated(): Observable<ApiResponse<boolean>> {
    const url = `${this.servicePath}/AmIAuthenticated`;
    return this.http.get<ApiResponse<boolean>>(url);
  }

  getMyInfo(): Observable<ApiResponse<AccountModel>> {
    const url = `${this.servicePath}/GetMyInfo`;
    return this.http.get<ApiResponse<AccountModel>>(url);
  }

  /* getMyMenu(): Observable<APIResponse<MenuModel[]>> {
    const url = `${ServerPath}${this.servicePath}GetMyMenu`;
    return this.http.post<APIResponse<Array<MenuModel>>>(url, null);
  } */
}
