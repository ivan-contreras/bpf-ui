import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private authservice: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authservice.amIAuthenticated()
    .pipe(
      map(response => {
        if (!response.hasError) {
          return response.result;
        } else {
          return false;
        }
      }),
      catchError(
        () => {
          // this.router.navigate(['notfound']);
          return of(false);
        }
      )
    );
  }
}
