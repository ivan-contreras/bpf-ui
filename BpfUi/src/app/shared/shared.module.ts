import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule, MatFormFieldModule, MatInputModule, MatDialogModule, MatButtonModule,
    MatSlideToggleModule, MatSelectModule, MatOptionModule, MatTableModule, MatCheckboxModule,
    MatNativeDateModule, MatDatepickerModule, MatToolbarModule, MatSidenavModule, MatListModule,
    MatCardModule, MatExpansionModule, MatGridListModule, MatChipsModule, MatAutocompleteModule,
    MatTabsModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule} from '@angular/material';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AlertComponent } from './components/alert/alert.component';
import { PlaceholderDirective } from './directives/placeholder.directive';
import { SpinnerOverlayComponent } from './components/spinner-overlay/spinner-overlay.component';
import { SpinnerComponent } from './components/spinner/spinner.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    /* Custom Components & Directives, */
    PlaceholderDirective,
    AlertComponent,
    SpinnerComponent,
    SpinnerOverlayComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    /* Custom Components & Directives, */
    PlaceholderDirective,
    AlertComponent,
    SpinnerComponent,
    SpinnerOverlayComponent,
    /* Angular Material Components */
    MatInputModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatDialogModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatExpansionModule,
    MatGridListModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule
  ],
  entryComponents: [
    SpinnerOverlayComponent,
    AlertComponent
  ]
})
export class SharedModule { }
