import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxSimpleSelectComponent } from './ngx-simple-select/ngx-simple-select.component';
import { NgxServerListComponent } from './ngx-server-select/ngx-server-list.component';

const testRoutes: Routes = [
  { path: 'ngx-simple-select', component: NgxSimpleSelectComponent },
  { path: 'ngx-server-select', component: NgxServerListComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(testRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TestRoutingModule { }
