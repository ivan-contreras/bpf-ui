import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ClientListComponent } from './client-list.component';
import { ClientRoutingModule } from './client-routing.module';

@NgModule({
  declarations: [
    ClientListComponent
  ],
  imports: [
    ClientRoutingModule,
    SharedModule
  ]
})
export class ClientModule { }
