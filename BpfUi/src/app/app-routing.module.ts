import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegularLayoutComponent } from './core/layout/regular-layout/regular-layout.component';
import { AuthGuardService } from './core/auth/auth-guard.service';
import { PageNotFoundComponent } from './modules/page-not-found/page-not-found.component';
import { LoginComponent } from './modules/login/login.component';
import { HomeComponent } from './modules/home/home.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: RegularLayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/login', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'clients', loadChildren: './modules/clients/client.module#ClientModule' },
      { path: 'tests', loadChildren: './modules/tests/test.module#TestModule' },
      { path: '**', component: PageNotFoundComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { enableTracing: false })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
