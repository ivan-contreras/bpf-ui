import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { GlobalHelper } from '../../shared/global-helper';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
    constructor(private spinner: NgxSpinnerService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spinner.show();

        const token = sessionStorage.getItem(GlobalHelper.tokenName);
        const authReq = req.clone({
            setHeaders: {
                Authorization: token != null ? `Bearer ${token}` : '',
                'Content-Type': 'application/json'
            }
        });

        // send the newly created request
        return next.handle(authReq).pipe(
            finalize(() => {
                this.spinner.hide();
            }),

            catchError((error, caught) => {
                // intercept the response error and displace it to the console
                console.log(error);
                this.handleAuthError(error);
                return of(error);
            }) as any
        );
    }

    private handleAuthError(err: HttpErrorResponse): Observable<any> {
        this.spinner.hide();

        // handle your auth error or rethrow
        if (err.status === 401 || err.status === 403) {
            // navigate /delete cookies or whatever
            console.log('handled error ' + err.status);
            // this.router.navigate(['/login']);
            // window.open('/login', '_self');
            /* if you've caught/handled the error, you don't want to rethrow
              it unless you also want downstream consumers to have to handle it as well. */
            return of(err.message);
        }
        // when theres no internet connection
        // window.open('/login', '_self');
        return of(err);
    }
}
