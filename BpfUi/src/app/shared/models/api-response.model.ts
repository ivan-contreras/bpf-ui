export class ApiResponse<T> {
    result: T = null;
    hasError = false;
    message = '';
}
