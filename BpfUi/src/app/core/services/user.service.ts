import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { GlobalHelper } from '../../shared/global-helper';
import { ApiResponse } from '../../shared/models/api-response.model';
import { UserModel } from '../models/user.model';
import { TableData } from 'src/app/shared/models/table-data.model';
import { SelectItem } from 'src/app/shared/models/select-item';
import { PagingData } from 'src/app/shared/models/paging-data';

@Injectable()
export class UserService {
  private servicePath = `${GlobalHelper.serverPath}User`;

  constructor(private http: HttpClient) { }

  getByFilter(pagingData: PagingData): Observable<ApiResponse<TableData<UserModel>>> {
    const url = `${this.servicePath}/GetByFilter`;
    const parameters = {
      filter: pagingData.filter,
      sortOrder: pagingData.sortOrder,
      sortColumn: pagingData.sortColumn,
      pageNumber: pagingData.pageNumber.toString(),
      pageSize: pagingData.pageSize.toString()
    };
    return this.http.get<ApiResponse<TableData<UserModel>>>(url, { params: parameters });
  }

  getSelectItemsByFilter(filter: string = ''): Observable<ApiResponse<SelectItem[]>> {
    const url = `${this.servicePath}/GetSelectItemsByFilter/${filter}`;
    return this.http.get<ApiResponse<SelectItem[]>>(url);
  }

  getById(id: number): Observable<ApiResponse<UserModel>> {
    const url = `${this.servicePath}/${id}`;
    return this.http.get<ApiResponse<UserModel>>(url);
  }
}
