import { Component, OnInit } from '@angular/core';
import { LoginModel } from './login.model';
import { AuthService } from '../../core/auth/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalHelper } from '../../shared/global-helper';
import { NotifierService } from '../../core/services/notifier.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private notifierService: NotifierService) { }

  ngOnInit() {
    // sessionStorage.removeItem(GlobalHelper.tokenName);
    sessionStorage.clear();
    this.initializeLoginForm();
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      this.notifierService.showToasterMessage(true, 'Please enter Username and Password.');
      return;
    }

    const userCredentials: LoginModel = this.getLoginModel();

    this.authService.loginUser(userCredentials).subscribe(data => {
      if (!data.hasError) {
        sessionStorage.setItem(GlobalHelper.tokenName, data.result.token);
        this.router.navigate(['home']);
      } else {
        this.notifierService.showToasterMessage(true, data.message);
      }
    });
  }

  private getLoginModel(): LoginModel {
    const model: LoginModel = new LoginModel();
    model.userName = this.loginForm.value.userName;
    model.password = this.loginForm.get('password').value;
    return model;
  }

  private initializeLoginForm() {
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }
}
