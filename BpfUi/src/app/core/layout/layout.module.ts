import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { RegularLayoutComponent } from './regular-layout/regular-layout.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    RegularLayoutComponent
  ],
  imports: [
    RouterModule,
    SharedModule
  ]
})
export class LayoutModule { }
