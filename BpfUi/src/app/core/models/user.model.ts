export class UserModel {
    id = 0;
    email: string;
    fullName: string;
    firstName: string;
    lastName: string;
}
