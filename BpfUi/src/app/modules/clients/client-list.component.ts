import { Component, OnInit, ComponentFactoryResolver, ViewChild, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UtilService } from '../../core/services/util.service';
import { Subscription, merge, fromEvent } from 'rxjs';
import { PlaceholderDirective } from 'src/app/shared/directives/placeholder.directive';
import { AlertComponent } from 'src/app/shared/components/alert/alert.component';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { UserService } from 'src/app/core/services/user.service';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { UserDataSource } from 'src/app/core/data-sources/user.datasource';
import { PagingData } from 'src/app/shared/models/paging-data';
import { GlobalHelper } from 'src/app/shared/global-helper';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];


export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit, AfterViewInit, OnDestroy {
  foods: Food[] = [
    { value: '0', viewValue: 'Filete' },
    { value: '1', viewValue: 'Pizza' },
    { value: '2', viewValue: 'Tacos' },
    { value: '3', viewValue: 'Gallo Pinto' },
    { value: '4', viewValue: 'Lomo Saltado' },
    { value: '5', viewValue: 'Hamburguesas' },
    { value: '6', viewValue: 'Ensalada' }
  ];

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  /* {static: true} */
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /* ServerSide DataTable */
  userDataSource: UserDataSource;
  userDisplayedColumns = ['id', 'fullName'];
  @ViewChild(MatPaginator) userPaginator: MatPaginator;
  @ViewChild(MatSort) userSort: MatSort;
  @ViewChild('userInput') userInput: ElementRef;
  private pagingData: PagingData;
  /* ServerSide DataTable */

  /* {static: false} */
  @ViewChild(PlaceholderDirective) alertHost: PlaceholderDirective;
  private closeSubscription: Subscription;

  employeeForm: FormGroup;

  submitted = false;
  isSaving = false;
  formErrors = {};

  validationMessages = {
    name: {
      required: 'Name is required.',
      minlength: 'Full Name must be greater or equal to 2 characters.'
    },
    lastName: {
      required: 'Last Name is required.',
      minlength: 'Last Name must be greater or equal to 2 characters.'
    }
  };

  constructor(private formBuilder: FormBuilder,
              private componentFactoryResolver: ComponentFactoryResolver,
              private utilService: UtilService,
              private userService: UserService,
              private spinnerService: SpinnerService) {}

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

    /* ServerSide DataTable */
    this.userDataSource = new UserDataSource(this.userService);

    // Error: ExpressionChangedAfterItHasBeenCheckedError if loadData in OnInit child
    /* setTimeout(() => {
      this.userDataSource.loadUsers();
    }); */
    this.pagingData = new PagingData();
    this.userDataSource.loadUsers(this.pagingData);

    /* ServerSide DataTable */

    this.employeeForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]]
    });
  }

  /* ServerSide DataTable */

  ngAfterViewInit() {
    // server-side search
    fromEvent(this.userInput.nativeElement, 'keyup')
      .pipe(
        debounceTime(GlobalHelper.tableConfig.debounceTime),
        distinctUntilChanged(),
        tap(() => {
          this.userPaginator.pageIndex = 0;
          this.loadUsersPage();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.userSort.sortChange.subscribe(() => this.userPaginator.pageIndex = 0);

    // on sort or paginate events, load a new page, will be triggered in two case because of merge operator
    merge(this.userSort.sortChange, this.userPaginator.page)
      .pipe(tap(() => this.loadUsersPage()))
      .subscribe();
  }

  private loadUsersPage() {
    this.pagingData.filter = this.userInput.nativeElement.value;
    this.pagingData.sortOrder = this.userSort.direction;
    this.pagingData.sortColumn = this.userSort.active;
    this.pagingData.pageNumber = this.userPaginator.pageIndex;
    this.pagingData.pageSize = this.userPaginator.pageSize;
    this.userDataSource.loadUsers(this.pagingData);
  }

  /* ServerSide DataTable */

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onRowClicked(row) {
    console.log('Row clicked: ', row);
  }

  onSubmit() {
    this.submitted = true;
    this.logValidationErrors();
    if (this.employeeForm.invalid) {
      return;
    }

    console.log('Created!');
  }

  logValidationErrors() {
    this.utilService.logValidationErrors(this.employeeForm, this.validationMessages, this.submitted, this.formErrors);
  }

  private showModal(message: string) {
    const alertComponentFactory = this.componentFactoryResolver.resolveComponentFactory(AlertComponent);
    const hostViewContainerRef = this.alertHost.viewContainerRef;
    hostViewContainerRef.clear();
    const componentRef = hostViewContainerRef.createComponent(alertComponentFactory);
    componentRef.instance.message = message;
    this.closeSubscription = componentRef.instance.close.subscribe(() => {
      this.closeSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
  }

  ngOnDestroy() {
    if (this.closeSubscription) {
      this.closeSubscription.unsubscribe();
    }
  }
}
