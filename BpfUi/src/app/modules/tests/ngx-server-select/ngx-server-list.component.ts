import { Component, OnInit, ComponentFactoryResolver, ViewChild, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { UserService } from 'src/app/core/services/user.service';
import { tap, debounceTime, distinctUntilChanged, takeUntil, take, filter, map, delay, finalize } from 'rxjs/operators';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material';
import { SelectItem } from 'src/app/shared/models/select-item';
import { NotifierService } from 'src/app/core/services/notifier.service';
import { GlobalHelper } from 'src/app/shared/global-helper';

@Component({
  selector: 'app-ngx-server-list',
  templateUrl: './ngx-server-list.component.html',
  styleUrls: ['./ngx-server-list.component.css']
})
export class NgxServerListComponent implements OnInit, OnDestroy, AfterViewInit {

  /** control for the selected bank for server side filtering */
  public bankServerSideCtrl: FormControl = new FormControl();

  /** control for filter for server side. */
  public bankServerSideFilteringCtrl: FormControl = new FormControl();

  /** indicate search operation is in progress */
  public isSelectFetchingData = false;

  /** list of banks filtered after simulating server side search */
  public  filteredServerSideBanks: ReplaySubject<SelectItem[]> = new ReplaySubject<SelectItem[]>(1);

  @ViewChild('serverSelect') serverSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  constructor(private formBuilder: FormBuilder,
              private componentFactoryResolver: ComponentFactoryResolver,
              private userService: UserService,
              private spinnerService: SpinnerService,
              private notifierService: NotifierService) {}

  ngOnInit() {

    // set initial selection
    this.bankServerSideCtrl.setValue({id: null});

    // load the initial bank list
    /* this.filteredServerSideBanks.next([
      {name: 'Bank E (France)', id: 'E'}
    ]); */

    // listen for search field value changes
    this.bankServerSideFilteringCtrl.valueChanges
      .pipe(
        /* filter(search => !!search), */
        tap(() => this.isSelectFetchingData = true),
        takeUntil(this._onDestroy),
        debounceTime(GlobalHelper.selectConfig.debounceTime),
        delay(GlobalHelper.selectConfig.delay)
      )
      .subscribe(search => {
        if (search) {
          this.userService.getSelectItemsByFilter(search).subscribe(data => {
            this.isSelectFetchingData = false;
            if (!data.hasError) {
              if (data.result != null && data.result.length === 0) {
                this.notifierService.showToasterErrorMessage('No matching item found.');
              }
              this.filteredServerSideBanks.next(data.result);
            } else {
              this.filteredServerSideBanks.next([]);
              this.notifierService.showToasterErrorMessage('No matching item found.');
            }
          });
        } else {
          this.isSelectFetchingData = false;
        }
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  protected setInitialValue() {
    this.filteredServerSideBanks
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredBanks are loaded initially
        // and after the mat-option elements are available
        // Only create the method if you are setting a default value
        /* this.singleSelect.compareWith = (a: Bank, b: Bank) => a && b && a.id === b.id; */
        this.serverSelect.compareWith = (a: SelectItem, b: any) => a.id === b.id;
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
}
