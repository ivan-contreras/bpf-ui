import { GlobalHelper } from '../global-helper';

export class PagingData {
    filter = '';
    sortOrder = 'asc';
    sortColumn = '';
    pageNumber = 0;
    pageSize = GlobalHelper.tableConfig.pageSize;
}
