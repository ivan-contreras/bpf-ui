import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { AuthService } from './core/auth/auth.service';
import { NotifierService } from './core/services/notifier.service';
import { AuthGuardService } from './core/auth/auth-guard.service';
import { AppHttpInterceptor } from './core/interceptors/app-http-interceptor.service';
import { SpinnerService } from './core/services/spinner.service';
import { OverlayModule } from '@angular/cdk/overlay';
import { UtilService } from './core/services/util.service';
import { PageNotFoundComponent } from './modules/page-not-found/page-not-found.component';
import { LoginModule } from './modules/login/login.module';
import { LayoutModule } from './core/layout/layout.module';
import { HomeModule } from './modules/home/home.module';
import { UserService } from './core/services/user.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TestModule } from './modules/tests/test.module';

const toastrConfig = {
  timeOut: 3000,
  positionClass: 'toast-top-right',
  /* positionClass: 'toast-center-center', */
  preventDuplicates: true,
  closeButton: true
};

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    OverlayModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(toastrConfig),
    /* App Modules */
    AppRoutingModule,
    LayoutModule,
    LoginModule,
    HomeModule,
    TestModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true },
    UtilService,
    SpinnerService,
    NotifierService,
    AuthService,
    AuthGuardService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
