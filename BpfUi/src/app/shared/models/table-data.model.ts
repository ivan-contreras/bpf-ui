export class TableData<T> {
    pageRecords: Array<T> = null;
    totalRecords = 0;
}
