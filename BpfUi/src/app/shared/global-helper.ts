import { environment } from '../../environments/environment';

export class GlobalHelper {
  static serverPath: string = environment.serverPath;
  static appName = 'BPF';
  static tokenName = 'bpfToken';

  static tableConfig = {
    debounceTime: 1000,
    pageSize: 5,
    pageSizeOptions: [3, 5, 10]
  };

  static selectConfig = {
    debounceTime: 1000,
    delay: 500
  };

  static getTodayTimeString(): string {
    return new Date().getTime().toString();
  }
}
