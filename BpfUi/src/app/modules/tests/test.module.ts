import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { TestRoutingModule } from './test-routing.module';
import { NgxSimpleSelectComponent } from './ngx-simple-select/ngx-simple-select.component';
import { NgxServerListComponent } from './ngx-server-select/ngx-server-list.component';

@NgModule({
  declarations: [
    NgxSimpleSelectComponent,
    NgxServerListComponent
  ],
  imports: [
    TestRoutingModule,
    SharedModule
  ]
})
export class TestModule { }
