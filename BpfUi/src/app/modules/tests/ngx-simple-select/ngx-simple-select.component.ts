import { Component, OnInit, ComponentFactoryResolver, ViewChild, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { UserService } from 'src/app/core/services/user.service';
import { tap, debounceTime, distinctUntilChanged, takeUntil, take } from 'rxjs/operators';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material';

export interface Bank {
  id: string;
  name: string;
}

export const BANKS: Bank[] = [
  {name: 'Bank A (Switzerland)', id: 'A'},
  {name: 'Bank B (Switzerland)', id: 'B'},
  {name: 'Bank C (France)', id: 'C'},
  {name: 'Bank D (France)', id: 'D'},
  {name: 'Bank E (France)', id: 'E'},
  {name: 'Bank F (Italy)', id: 'F'},
  {name: 'Bank G (Italy)', id: 'G'},
  {name: 'Bank H (Italy)', id: 'H'},
  {name: 'Bank I (Italy)', id: 'I'},
  {name: 'Bank J (Italy)', id: 'J'},
  {name: 'Bank Kolombia (United States of America)', id: 'K'},
  {name: 'Bank L (Germany)', id: 'L'},
  {name: 'Bank M (Germany)', id: 'M'},
  {name: 'Bank N (Germany)', id: 'N'},
  {name: 'Bank O (Germany)', id: 'O'},
  {name: 'Bank P (Germany)', id: 'P'},
  {name: 'Bank Q (Germany)', id: 'Q'},
  {name: 'Bank R (Germany)', id: 'R'}
];

@Component({
  selector: 'app-ngx-simple-select',
  templateUrl: './ngx-simple-select.component.html',
  styleUrls: ['./ngx-simple-select.component.css']
})
export class NgxSimpleSelectComponent implements OnInit, OnDestroy, AfterViewInit {

  /** list of banks */
  protected banks: Bank[] = BANKS;

  /** control for the selected bank */
  public bankCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public bankFilterCtrl: FormControl = new FormControl();

  /** list of banks filtered by search keyword */
  public filteredBanks: ReplaySubject<Bank[]> = new ReplaySubject<Bank[]>(1);

  @ViewChild('singleSelect') singleSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  constructor(private formBuilder: FormBuilder,
              private componentFactoryResolver: ComponentFactoryResolver,
              private userService: UserService,
              private spinnerService: SpinnerService) {}

  ngOnInit() {
    // set initial selection
    this.bankCtrl.setValue({id: null});

    // load the initial bank list
    this.filteredBanks.next(this.banks.slice());

    // listen for search field value changes
    this.bankFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBanks();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  protected filterBanks() {
    if (!this.banks) {
      return;
    }
    // get the search keyword
    let search = this.bankFilterCtrl.value;
    if (!search) {
      this.filteredBanks.next(this.banks.slice());
    } else {
      search = search.toLowerCase();
      // filter the banks
      this.filteredBanks.next(
        this.banks.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
      );
    }
  }

  /**
   * Sets the initial value after the filteredBanks are loaded initially
   */
  protected setInitialValue() {
    this.filteredBanks
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredBanks are loaded initially
        // and after the mat-option elements are available
        // Only create the method if you are setting a default value
        /* this.singleSelect.compareWith = (a: Bank, b: Bank) => a && b && a.id === b.id; */
        this.singleSelect.compareWith = (a: Bank, b: any) => a.id === b.id;
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
}
