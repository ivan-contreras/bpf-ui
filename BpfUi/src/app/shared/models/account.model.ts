export class AccountModel {
  id = 0;
  email: string;
  fullName: string;
  firstName: string;
  lastName: string;
  picture: string;
  token: string;
}
