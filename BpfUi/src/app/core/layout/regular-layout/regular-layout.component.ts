import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/auth/auth.service';
import { AccountModel } from 'src/app/shared/models/account.model';
import { GlobalHelper } from 'src/app/shared/global-helper';
import { Router, Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-regular-layout',
  templateUrl: './regular-layout.component.html',
  styleUrls: ['./regular-layout.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class RegularLayoutComponent implements OnInit {

  appName = GlobalHelper.appName;
  accountInfo: AccountModel;
  toggleSideBar = false;

  constructor(private router: Router,
              private authService: AuthService,
              private spinnerService: NgxSpinnerService) {
    /* this.router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.spinnerService.show();
      } else if (routerEvent instanceof NavigationEnd ||
        routerEvent instanceof NavigationCancel ||
        routerEvent instanceof NavigationError) {
          this.spinnerService.hide();
      }
    }); */
  }

  ngOnInit() {
    this.authService.getMyInfo().subscribe(data => {
      if (!data.hasError) {
        this.accountInfo = data.result;
      }
    });
  }

  logout() {
    this.spinnerService.show();
    sessionStorage.clear();
    this.router.navigate(['login']);
    this.spinnerService.hide();
  }

  onToggleSideBar() {
    this.toggleSideBar = !this.toggleSideBar;
  }
}
